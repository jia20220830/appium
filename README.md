## 项目介绍：

使用Appium对 android APP界面、功能进行验证，技术框架appium+pytest+allure
## appium运行原理：
![APPIUM.png](config%2FAPPIUM.png)
- 4723：appium 服务器端口，监听脚本发送的指令
- 4724：appium1.13版本之前appium在PC端占用的端口
- 8200：appium1.13版本之后在PC端的监听端口，和手机端建立端口转发
- 8200-8299：uiautomator2端口范围
- 6790：手机端监听端口
- 脚本通过4723端口发送命令给appium server，appium server 与手机端通过8200--->6790端口建立通讯转发
## 技术特点：
- 关键字驱动

- 使用yaml、excel.csv编辑测试用例

- allure自动生成测试报告

- 使用pytest库插件可实现测试用例失败生跑，跳过不需要执行的测试用例，执行指定标记的测试用例

- 使用pytest库后，_init_初始化方法将不可用

- 模块名以test_开头或_test结尾；类名以Test开头；方法名以test开头，可在pytest.ini中自定义

## 页面展示：
![image](https://user-images.githubusercontent.com/83941545/203574262-9bcbbcb9-db37-4aee-b01a-6e59419e670a.png)
![image](https://user-images.githubusercontent.com/83941545/203574477-a0efa1f5-f0b3-4edd-87b7-d76289ae4fd8.png)

## 安装环境：
- win 10

- python 3.9

- pytest 6.2.4

- allure 2.9.43

- flask 2.0.1

- html 3.1.1

## 如何安装：
pip install -r requirement_plugin.txt 安装所有使用插件

## 插件介绍：

- pytest-ordering  指定执行顺序


- pytest-xdist     多线程运行用例


- pytest-rerunfailures 失败重跑


- pytest-html   输出报告


- allure_report-pytest  美化报告


- fixture     用例的前置和后置

## 目录介绍：
--Data: ------------->测试数据

-- Log: ------------->日志

-- Report: --------->测试报告

-- Run ------------->用例执行入口

-- common  ------->通用自定义方法

-- config ----------->配置文件

-- screenshot ---->截图图片

-- test_case  ------>测试用例 

-- .gitignore ------->更新git时忽略的文件

-- LICENSE ------->开源证书

-- README ------->项目说明

-- conftest  --------->配合fixture做用例的前置和后置方法

-- pytest.ini  -------->pytest的配置文件

--pytest_note.md ->pytest使用说明

-- requirements.txt  ->需要安装的第三方库

-- shortCut_key  ---->pycharm快捷键，git使用说明

## 微信赞赏：
![输入图片说明](screenshots/%E8%B5%9E%E8%B5%8F.png)