# _*_ coding:utf-8 _*_
"""
@Version:  python3.9
@Project: appium
@File:   conftest.PY
@Date:  2022/5/26 23:07
@Author: jia
@Description:  做为多个.PY文件的全局共享变量，或方法

"""
import pytest
import yaml
from appium import webdriver


@pytest.fixture(scope='session')
def desired():
    """
    appium初始化参数
    :return:返回driver实例对象
    """
    # 读取YAML配置文件
    with open('../Data/emulator.yaml', 'r', encoding='utf-8') as file:
        data = yaml.full_load(file)  # yaml 5.1新用法

    # 初始化配置数据
    desired_caps = dict()
    desired_caps['platformName'] = data['platformName']
    desired_caps['platformVersion'] = data['platformVersion']
    desired_caps['deviceName'] = data['deviceName']
    # 自动安装APP
    # desired_caps['app'] = data['app']
    desired_caps['appPackage'] = data['appPackage']
    desired_caps['appActivity'] = data['appActivity']
    # false,执行的操作会重置，true不重置
    desired_caps['noReset'] = data['noReset']
    # 输入中文需要设置
    desired_caps['unicodeKeyboard'] = data['unicodeKeyboard']
    desired_caps['resetKeyboard'] = data['resetKeyboard']
    # 检查toast需要uiautomator2
    desired_caps['automationName'] = data['automationName']
    # 启动APP
    driver = webdriver.Remote('http://' + str(data['ip']) + ':' + str(data['port']) + '/wd/hub', desired_caps)

    driver.implicitly_wait(10)
    return driver
