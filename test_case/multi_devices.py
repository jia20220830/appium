# _*_ coding:utf-8_*_
"""
@Project:   appium
@FileName:  multi_device.py
@CreateDate:2023/3/15 22:24
@Author:    Jia
@Desc:  启用多台设备
"""
from appium import webdriver
import yaml
from time import ctime


with open('../Data/emulator.yaml', 'r') as file:
    data = yaml.full_load(file)

devices_list = ['127.0.0.1:62001', '127.0.0.1:62026']


def appium_desired(udid, port, sysport):
    """
    初始化参数（设备为模拟器）
    :param udid: 设备唯一识别码
    :param port: appium服务的端口
    :param sysport:默认是8200，是appium server在pc的端口用于与手机建立通讯
    :return: 返回驱动对象实例
    """
    desired_caps = dict()
    desired_caps['platformName'] = data['platformName']
    desired_caps['platformVersion'] = data['platformVersion']
    desired_caps['deviceName'] = data['deviceName']
    desired_caps['automationName'] = data['automationName']
    desired_caps['systemPort'] = sysport
    desired_caps['udid'] = udid

    # desired_caps['app']=Data['app']
    desired_caps['appPackage'] = data['appPackage']
    desired_caps['appActivity'] = data['appActivity']
    desired_caps['noReset'] = data['noReset']

    print('appium port:%s start run %s at %s' % (port, udid, ctime()))
    driver = webdriver.Remote('http://'+str(data['ip'])+':'+str(port)+'/wd/hub', desired_caps)
    driver.implicitly_wait(8)

    return driver


if __name__ == '__main__':
    appium_desired(devices_list[0],4723)
    appium_desired(devices_list[1],4725)