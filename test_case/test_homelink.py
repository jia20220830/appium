# -*- coding:UTF-8 -*-
"""
@Project:   appium
@FileName:  test_homelink.py
@CreateDate:2023/3/22 20:40  
@Author:    Jia  
@Desc: 使用pytest测试homelink并生成测试报告

"""
from common import nox
from common import file_methods
from appium.webdriver.common.appiumby import AppiumBy
from config import log
import subprocess
import requests
import allure
import time


class TestHomelink:
    """
    测试homelinkingAPP
    """
    root_dir = r'E:/PycharmProjects/appium/'
    logger = log.log_execute('info_logger')
    driver = nox.homelink_driver()

    def setup_class(self):
        """
        向allure报告中写入环境配置说明
        """
        # allure报告中的环境配置变量
        en_text = 'System.version = win10\nauthor = jia\npython = 3.9.0\n' \
                  'pyTest = 7.2.2\nallure = 2.13.0\nflask = 2.0.1\nhtml = 3.1.1'

        # 先清除历史缓存后写入
        file_methods.clean_dir(self.root_dir + "Report/temp")
        file_methods.write_file(self.root_dir + "Report/temp/environment.properties", en_text)
        self.logger.info('向allure报告写入了配置说明')

    def setup_function(self):
        """
        启动appium服务
        bp: 8200-8299,—bootstrap-port是appium和设备之间通信的端口
        host_addr: 主机地址，本面
        port_num: 4723，appium服务器的端口
        """
        host_addr = '127.0.0.1'
        port_num = 4723
        bp = 8200
        cmd = 'start /b appium -a {} -p {} -bp {}'.format(host_addr, port_num, bp)
        # 在新进程中执行子进程
        subprocess.Popen(cmd, shell=True, stdout=open('../Log/' + 'appium_' + str(port_num) + '.log', 'a'),
                         stderr=subprocess.STDOUT)
        # 检查服务是否启动成功
        time.sleep(3)
        url = 'http://{}:{}/wd/hub/status'.format(host_addr, port_num)
        res = requests.get(url)
        if res.status_code == 200:
            self.logger.info('appium服务已启动')
        else:
            self.logger.info('APPIUM服务未启动成功')

    @allure.severity(allure.severity_level.BLOCKER)
    @allure.step('登录')
    @allure.title('登录homelinking')
    def test_login(self):
        """
        检查授权弹窗并登录
        :return:
        """
        # global driver
        driver = self.driver
        el1 = driver.find_element(AppiumBy.ID, "com.hle.lhzm:id/confirm_but")
        el1.click()
        el2 = driver.find_element(AppiumBy.ID, "com.hle.lhzm:id/confirm_but")
        el2.click()
        el3 = driver.find_element(AppiumBy.ID, "com.android.packageinstaller:id/permission_allow_button")
        el3.click()

    @allure.severity(allure.severity_level.BLOCKER)
    @allure.step('检查首页')
    @allure.title('检查首页')
    def test_home(self):
        """
        检查主页
        :return:
        """
        self.logger.info('检查主页')

    @allure.severity(allure.severity_level.BLOCKER)
    @allure.step('退出')
    @allure.title('退出登录')
    def test_logout(self):
        """
        退出登录
        :return:
        """
        self.logger.info('退出登录')

    def teardown_class(self):
        """
        生成allure报告
        """
        port_num = '4723'
        cmd = 'allure generate ../Report/temp -o ../Report/allure_report --clean'
        subprocess.Popen(cmd, shell=True, stdout=open('../Log/' + 'appium_' + port_num + '.log', 'a'),
                         stderr=subprocess.STDOUT)

        self.logger.info('生成allure测试报告')
