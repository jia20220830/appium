# Pytest
## 规则
- py文件以test_开头
- 类以test_开头
- 方法以test_开头
- --
## setup,teardown方法
- 模块级别：setup_module()、teardown_module()
- 函数级别：setup_function()、teardown_function()
- 类级别：setup_class()、teardown_class()
- 类方法级别：setup_method()、teardown_method()
- 类方法细化级别：setup()、teardown()
- --
## conftest.py用法
- 文件固定命名，不支持自定义
- 所有同目录测试文件运行前都会执行conftest.py文件
- 适用于多个py文件之间的数据共享
- 放在根目录配合fixture可做全局的前置和后置的方法来调用
- 放在package下，就在package内有效
---
## fixture用法
- session>module>class>function
- session：所有测试.py文件执行前执行一次
- module：每一个测试.py文件执行前都会执行一次
- class：每一个测试文件中的测试类执行前都会执行一次
- function：所有文件的测试用例执行前都会执行一次
- scope=session时要放在conftest.py下
- 示例
  >@pytest.fixture(scope='class', autouse=False)
  >>def desired():
  >>> print('这是fixture方法')

- autouse用法
  >@pytest.fixture(scope='function', autouse=True)
  >> autouse=true时不用在每个方法中调用fixture,会自动在每个方法执行前调用

- yieid用法
  >@pytest.fixture(scope='class', autouse=False)
  >>def desired():
  > 
  >>> driver = webdriver()
  > 
  >>> yieid driver # yieid之后会在类执行完再执行，相当于teardown_class
  > 
  >>> print('关闭')
  > 
  >>> driver.close()
---
## pytest.ini
- 主配置文件，优先级最高，可以修改pytest默认值
- addopts： 命令行参数
- testpaths: 测试路径，命令行没有输入此参数时才会用到这个默认参数
- markers：注册测试用例的种类，方便分批执行
- filterwarnings: 过滤掉警告信息
- --
## pytest.main
-  -vs 详细输出；
- --reruns=2 失败重跑2次；
- -x 失败停止执行；
- -maxfail=2 失败2个就停止
- -k 'xi and test' 只执行用例名包含字符xi和test的用例; 
- -n 2 多线程运行
- -m=smoke or retest 执行两种自定义标记的用例
- 执行smoke标记或retest标记的用例，失败重跑2次
  >>pytest.main(['-vs', '-m=smoke or retest', '--reruns=2'])
- 执行这个目录下所有用例，并指定测试报告目录
  >>pytest.main(['-s', '../test_case/', '--alluredir', '../Report/temp'])
- 生成测试报告
  >>os.system('allure generate ../Report/temp -o ../Report/allure_report --clean')
---
## terminal
- pycharm的terminal下执行用例
  >> pytest -s test_case/test_homelink.py  --alluredir=Report/temp

- 生成测试报告
  >>allure generate Report/temp -o Report/allure_report --clean
---
## allure报告
- 用于在dos下输出测试报告
- [官方地址](https://docs.qameta.io/allure/#_installing_a_commandline)打开后点击Maven Central
- 下载ZIP文件解压后将bin文件目录加入环境变量
- allure --version查看版本