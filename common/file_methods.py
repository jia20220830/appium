# _*_ coding:utf-8 _*_
"""
@Version: python3.9
@Project: appium
@File: file_method.PY
@Date: 2022/5/28 21:03
@Author: jia
@Description:自定义函数，对yaml,CSV文件进行增，删，改
对于公共变量YAML可以将数据写在开头，并用&来标记为锚，用*号来引用
对于多个文档yaml可以用--来分隔
对于一组中的多个元素可以用-来分隔
"""
import csv
import os
import shutil
import yaml


def clean_dir(dir_path):
    """
    清除后创建新的文件目录
    :param dir_path: 文件夹目录
    """
    if not os.path.exists(dir_path):
        os.mkdir(dir_path)
    else:
        shutil.rmtree(dir_path)
        os.mkdir(dir_path)


def get_project_path(project_name):
    """
    取项目根目录
    :param project_name: 输入项目名称
    :return: 返回项目根目录
    """
    base_dir = os.getcwd()
    root_path = base_dir[:base_dir.find(project_name + "\\") + len(project_name + "\\")]
    project_path = root_path.replace("\\", '/')
    print('project_path:', project_path)
    return project_path


def write_file(file_path, write_text):
    """
    向文件写入
    :param file_path: 文件路径
    :param write_text: 要写入的内容
    """
    with open(file_path, 'w', encoding='utf-8') as wf:
        wf.write(write_text)


def read_yaml(f_path):
    """
    读取yaml文件数据
    :param f_path: 要读取的yaml文件路径
    :return: 返回读取到的内容,字典类型
    """
    with open(f_path, 'r', encoding='utf-8') as f:
        ff = yaml.load(f, Loader=yaml.FullLoader)
        return ff


def write_yaml(f_path, f_data):
    """
    向YAML文件写入数据
    :param f_path: 文件 路径
    :param f_data: 写入内容, 字典类型
    """
    with open(f_path, 'w', encoding='utf-8') as wf:
        yaml.dump(f_data, stream=wf)


def clear_yaml(f_path):
    """
    将yaml文件内容清除
    :param f_path: 文件路径
    """
    with open(f_path, 'r+', encoding='utf-8') as cf:
        cf.truncate()


def read_extract_yaml(project_name, one_name):
    """
    取根目录中extract文件的值
    :param project_name: 项目名称
    :param one_name: 要取值的KEY
    :return: 返回KEY的VALUE
    """
    root = get_project_path(project_name)
    with open(str(root + 'extract.yaml'), 'r', encoding='utf-8') as rf:
        extract = yaml.load(rf.read(), Loader=yaml.FullLoader)
        return extract[one_name]


def test_modify(f_path, in_para, one_name, two_name):
    """
    修改YAML文件参数
    f_path: 打开文件的路径
    in_para: 输入的参数
    one_name: 字典第一个key
    two_name: 字典第二个kdy
    """
    with open(f_path) as f:
        doc = yaml.safe_load(f)
        doc[one_name][two_name] = in_para
    with open(f_path, 'w') as wf:
        yaml.safe_dump(doc, wf, allow_unicode=True)


def get_csv_data(csv_file, line):
    """
    获取CSV文件指定行的数据
    name    score
    Mike    61
    hero    89
    trump    90

    :param csv_file: CSV文件路径
    :param line: 第几行
    :return: 返回该行数据
    """
    with open(csv_file, 'rb') as file:
        reader = csv.reader(file) # reader是一个对象类型
        for index, row in enumerate(reader):  # enumerate对一个可迭代对象输出下标和值，默认初始下标值从0开始
            if index == line:
                return row # 列表类型 ['name', 'score']


def get_csv_row(csv_file, key, value):
    """
    获取CSV文件中，key= VALUE的那一行
    :param csv_file: 读取的文件
    :param key: 指定读取的key
    :param value: 指定读取的内容
    :return: 返回那一行数据
    """
    with open(csv_file, 'rb', encoding='utf-8') as f:
        reader = csv.DictReader(f)
        # line返回是字典类型
        for line in reader:
            if value == line[key]:
                return line
