# _*_coding:utf-8_*_
"""
@Project:   appium
@FileName:  demo_wait.py
@CreateDate:2023/3/15 22:24
@Author:    Jia
@Desc:  appium的三种等待方式
"""
from common.emulator_desired import desired
from time import sleep
from selenium.webdriver.support.ui import WebDriverWait
driver = desired()

'''隐式等待，针对全部元素设置的等待时间,智能等待'''
driver.implicitly_wait(5)

'''强制等待 X秒，线程休眠'''
sleep(2)

"""
显示等待，针对某个特定元素等待
(self, driver, timeout, poll_frequency=POLL_FREQUENCY, ignored_exceptions=None):
poll_frequency:每隔一段时间检测一次，默认0.5秒
ignored:超时报错信息
"""
WebDriverWait(driver, 3).until(lambda x: x.find_element_by_id('com.tal.kaoyan:id/mainactivity_button_forum'))


