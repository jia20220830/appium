# _*_coding:utf-8_*_
"""
@Project:   appium
@FileName:  multi_devices.py
@CreateDate:2023/3/15 22:24
@Author:    Jia
@Desc:  启动多个设备
"""
from common.check_port import *
import multiprocessing

devices_list = ['127.0.0.1:62001', '127.0.0.1:62026']


def check_server_port(host, port):
    """
    端口是否被占用
    :param host:主机地址
    :param port: 端口号
    :return:
    """
    if check_port(host, port):
        return True
    else:
        print('appium %s start fail' % port)
        return False


def start_devices_action(udid, port, sysport):
    """
    如果appium服务器端口可用就启动服务器和APP,否则关闭占用端口
    :param udid:设备唯一识别码
    :param port: appium server端口
    :param sysport:
    """
    host = '127.0.0.1'
    if check_server_port(host, port):
        pass
        # 启动服务器
        # 初始化desired_caps
    else:
        release_port(port)


def appium_start_sync():
    """
    同时启动多个appium服务
    """
    pass


def devices_star_sync():
    """
    同时启动多个设备
    """
    print('======devices_star_sync===')

    desired_process = []

    for i in range(len(devices_list)):
        port = 4723 + 2 * i
        sysport = 8200 + i
        desired = multiprocessing.Process(target=start_devices_action, args=(devices_list[i], port, sysport))
        desired_process.append(desired)

    for desired in desired_process:
        desired.start()
    for desired in desired_process:
        desired.join()


if __name__ == '__main__':
    appium_start_sync()
    devices_star_sync()
