# -*- coding:UTF-8 -*-
"""
@Project:   appium
@FileName:  nox.py
@CreateDate:2023/3/23 22:11  
@Author:    Jia  
@Desc:  用夜神模拟器启动homelinking APP
"""
from appium import webdriver


def homelink_driver():
    """
    初始化配置参数

    caps['app'] = '', 从指定路径安装APP到手机
    caps['noReset'] = True, 不重置当前保存的状态,False:会话结束后重置当前保存状态
    caps['unicodeKeyboard'] = True, 输入中文需要设置
    caps['resetKeyboard'] = True, 会话结束后将键盘重置为默认状态
    caps['automationName'] = 'uiautomator2', 检查toast需要uiautomator2 ,引擎默认是appium
    """
    caps = dict()
    caps['platformName'] = 'android'
    caps['platformVersion'] = '7.1.2'
    caps['deviceName'] = 'emulator_nox7'
    caps['appPackage'] = 'com.hle.lhzm'
    caps['appActivity'] = 'cn.hle.lhzm.ui.activity.main.SplashActivity'
    caps['automationName'] = 'uiautomator2'
    caps['noReset'] = False
    caps['unicodeKeyboard'] = True
    caps['resetKeyboard'] = True

    # 启动APP
    driver = webdriver.Remote('http://127.0.0.1:4723/wd/hub', caps)

    # 隐式智能等待，针对全部元素
    driver.implicitly_wait(8)

    return driver
