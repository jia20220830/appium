# _*_coding:utf-8_*_
"""
@Project:   appium
@FileName:  multi_appium.py
@CreateDate:2023/3/15 22:24
@Author:    Jia
@Desc:  命令启动appium服务
脚本通过4723端口发送给APPIUM，因此需要先启动appium 服务，使之处于监听4723端口状态下
os.system(cmd):阻塞式，只返回命令的状态
os.popen(cmd,mode,bufsize):非阻塞，.read方法可读取str类型返回值
subprocess.Popen(cmd,shell=True,stdout='',stderr=subprocess.STDOUT):非阻塞，功能更强

"""
import subprocess
import time
from time import ctime
import requests
import os


def kill_process(port_n):
    """
    根据端口找到进程ID，然后关闭
    """
    # 找到端口为port的所有进程信息
    # 比如：TCP    127.0.0.1:4738         0.0.0.0:0              LISTENING       14520
    cmd = "netstat -aon|findstr " + str(port_n)
    p = str(os.popen(cmd).read())

    # 取第一行数据
    pid_str = p.split("\n")[0]

    """用空格切分为列表['TCP', '0.0.0.0:5555', '0.0.0.0:0', 'LISTENING', '12468']"""
    pid_l = pid_str.split()
    print(pid_l)

    if len(pid_l) == 0:
        print('端口没有被占用')
        return 0
    else:
        os.popen("taskkill /f /pid %s" % pid_l[-1])
        print('kill pid %s' % pid_l[-1])


def start_server(host_addr, port_num, bp):
    """
    启动appium服务，/b指后台运行

    :param bp: 8200-8299,—bootstrap-port是appium和设备之间通信的端口
    :param host_addr: 主机地址，本面
    :param port_num: 4723，appium服务器的端口
    """
    cmd = 'start /b appium -a {} -p {} -bp {}'.format(host_addr, port_num, bp)

    print('%s at %s' % (cmd, ctime()))

    subprocess.Popen(cmd, shell=True, stdout=open('../Log/' + 'appium_' + str(port_num) + '.log', 'a'),
                     stderr=subprocess.STDOUT)


def is_start(url, timeout):
    """
    检查APPIUM服务开启状态
    :param url: 服务器地址
    :param timeout: 超时时间
    :return:True 开启，False, 没有开启
    """
    start_time = time.time()
    while True:
        try:
            res = requests.get(url)
            if res.status_code == 200:
                print('已开启')
                return True
            else:
                end_time = time.time()
                if (end_time - start_time) > timeout:
                    print('请求超时')
                    return False
        except Exception as e:
            print('错误信息是' % e)


if __name__ == '__main__':
    # kill端口占用的pid进程
    # kill_process(5555)

    # 开启appium服务
    host = '127.0.0.1'
    port = 4723
    bp = 8200
    start_server(host, port, bp)

    # 检查是否开启成功
    # time.sleep(3)
    # host_url = 'http://{}:{}/wd/hub/status'.format(host, port)
    # is_start(host_url, 10)


