# _*_ coding:utf-8 _*_
"""
@Version: python3.9
@Project: appium
@FileName: action.PY
@Date: 2022/05/23 22:40
@Author: jia
@Description: 常用操作函数:上，下， 左，右划动，放大，缩小,截图
"""
from appium.webdriver.common.touch_action import TouchAction
from appium.webdriver.common.multi_action import MultiAction
import time
import os


def swipe_left(driver):
    """
    左划
    """
    x = driver.get_window_size()['width']
    y = driver.get_window_size()['height']

    x1 = int(x * 0.9)
    y1 = int(y * 0.5)
    x2 = int(x * 0.1)
    driver.swipe(x1, y1, x2, y1, 1000)


def swipe_up(driver):
    """
    上划
    """
    x = driver.get_window_size()['width']
    y = driver.get_window_size()['height']

    x1 = int(x * 0.5)
    y1 = int(y * 0.95)
    y2 = int(y * 0.35)
    driver.swipe(x1, y1, x1, y2, 1000)


def swipe_down(driver):
    """
    # 下划
    """
    x = driver.get_window_size()['width']
    y = driver.get_window_size()['height']

    x1 = int(x * 0.5)
    y1 = int(y * 0.35)
    y2 = int(y * 0.85)
    driver.swipe(x1, y1, x1, y2, 1000)


def swipe_right(driver):
    """
    # 右划
    """
    x = driver.get_window_size()['width']
    y = driver.get_window_size()['height']

    y1 = int(y * 0.5)
    x1 = int(x * 0.25)
    x2 = int(x * 0.95)
    driver.swipe(x1, y1, x2, y1, 1000)


def zoom_out(driver):
    """
    缩小
    :param driver:
    """
    x = driver.get_window_size()['width']
    y = driver.get_window_size()['height']

    action1 = TouchAction(driver)
    action2 = TouchAction(driver)
    pinch_action = MultiAction(driver)

    action1.press(x=x*0.2, y=y*0.2).wait(1000).move_to(x=x*0.4, y=y*0.4).wait(1000).release()
    action2.press(x=x*0.8, y=y*0.8).wait(1000).move_to(x=x*0.6, y=y*0.6).wait(1000).release()
    # mutiaction方法，ADD()添加对象，perform()执行
    pinch_action.add(action1, action2)
    pinch_action.perform()


def zoom_in(driver):
    """
    放大
    """
    x = driver.get_window_size()['width']
    y = driver.get_window_size()['height']

    action1 = TouchAction(driver)
    action2 = TouchAction(driver)
    zoom_action = MultiAction(driver)

    action1.press(x=x*0.4, y=y*0.4).wait(1000).move_to(x=x*0.2, y=y*0.2).wait(1000).release()
    action2.press(x=x*0.6, y=y*0.6).wait(1000).move_to(x=x*0.8, y=y*0.8).wait(1000).release()

    zoom_action.add(action1, action2)
    zoom_action.perform()


def screenshots(driver, save_path):
    """
    保存截图
    :param driver:
    :param save_path: 保存路径
    """
    # 获取年-月-日 时：分：秒
    now_time = time.strftime("%Y-%m-%d %H_%M_%S")

    # 获得当前文件所在目录绝对路径
    # currentPath = os.path.dirname(os.path.abspath(__file__))
    image_file = os.path.join(save_path + '%s.png' % now_time)
    driver.get_screenshot_as_file(image_file)






