# -*- coding:UTF-8 -*-
"""
@Project:   appium
@FileName:  demo_homelink.py
@CreateDate:2023/3/15 22:44  
@Author:    Jia  
@Desc:  演示示例，用雷电模拟器启动homelinking
"""
from appium import webdriver
from appium.webdriver.common.appiumby import AppiumBy


class Demo1:
    """
    启动，登录，退出
    """
    def __init__(self):
        """
        初始化配置参数

        caps['app'] = '', 从指定路径安装APP到手机
        caps['noReset'] = True, 不重置当前保存的状态,False:会话结束后重置当前保存状态
        caps['unicodeKeyboard'] = True, 输入中文需要设置
        caps['resetKeyboard'] = True, 会话结束后将键盘重置为默认状态
        caps['automationName'] = 'uiautomator2', 检查toast需要uiautomator2 ,引擎默认是appium
        """
        caps = dict()
        caps['platformName'] = 'android'
        caps['platformVersion'] = '9.0'
        caps['deviceName'] = 'emulator_leidian9'
        caps['appPackage'] = 'com.hle.lhzm'
        caps['appActivity'] = 'cn.hle.lhzm.ui.activity.main.SplashActivity'
        caps['automationName'] = 'uiautomator2'
        caps['noReset'] = False
        caps['unicodeKeyboard'] = True
        caps['resetKeyboard'] = True

        # 启动APP
        self.driver = webdriver.Remote('http://127.0.0.1:4723/wd/hub', caps)

        # 隐式智能等待，针对全部元素
        self.driver.implicitly_wait(8)

    def check_auth(self):
        """
        检查授权弹窗
        """
        # driver.find_element(AppiumBy.ID, value="com.hle.lhzm:id/confirm_but")
        el1 = self.driver.find_element("com.hle.lhzm:id/confirm_but")
        el1.click()
        el2 = self.driver.find_element("com.hle.lhzm:id/confirm_but")
        el2.click()
        el3 = self.driver.find_element("com.android.packageinstaller:id/permission_allow_button")
        el3.click()

    def logout(self):
        """
        退出登录
        """
        self.driver.close()


if __name__ == '__main__':
    d = Demo1()
    d.check_auth()
    d.logout()
