# _*_coding:utf-8_*_
"""
@Project:   appium
@FileName:  check_port.py
@CreateDate:2023/3/20 18:00
@Author:    Jia
@Desc:  检查商品是否被占用
"""
import socket
import os


def check_port(host, port):
    """
    尝试连接一个端口，连接失败表示此端口未打开未占用
    :param host: 主机IP
    :param port: 要检查的端口
    :return:
    """
    # 创建一个socket对象，
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    try:
        s.connect((host, port))
        s.shutdown(2)
    except OSError as msg:
        print('port %s is available!' % port)
        print(msg)
        return True
    else:
        print('port %s already be in use! ' % port)
        return False


def release_port(port):
    """
    找到占用此端口的PID进程，然后关闭它
    :param port: 要检查的端口
    """
    cmd_find = 'netstat -ano |findstr %s' % port
    print(cmd_find)
    # 读取命令执行后的结果
    result = os.popen(cmd_find).read()
    print(result)
    # 如果端口号和listening都存在说明有进程打开
    if str(port) and 'LISTENING' in result:
        # 得到listening的下标值
        i = result.index('LISTENING')
        # 加上7个空格得到pid的起始下标值
        start = i+len('LISTENING')+7
        # 获取换行符做为PID的末位下标值
        end = result.index('\n')
        # 截取字符串得到PID
        pid = result[start:end]

        cmd_kill = 'taskkill -f -pid %s' % pid
        print(cmd_kill)
        os.popen(cmd_kill)
    else:
        print('port %s is available' % port)


if __name__ == '__main__':
    h = '127.0.0.1'
    p = 4725
    check_port(h, p)
    # release_port(port)


