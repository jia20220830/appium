#!/usr/bin/env python
"""
@Version: python3.9
@Project: appium
@File: run.PY
@Date: 2022/5/28 21:22
@Author: jia
@Description: 运行测试用例统一入口

[0x7FFDA42FE0A4] ANOMALY: use of REX.w is meaningless (default operand size is 64)
命令行报以上错误时打开注册表“regedit”
HKEY_LOCAL_MACHINE/SOFTWARE/TEC/Ocular.3/agent/config.yaml.yml 添加二进制数值
hookapi_disins,数值数据: 1
 """

import pytest
import os
if __name__ == '__main__':

    #  控制台生成测试报告
    pytest.main(['-s', '../test_case/test_homelink.py', '--alluredir', '../Report/temp'])
    # os.system('allure generate ../Report/temp -o ../Report/allure_report --clean')

    # Terminal下执行
    # pytest -s test_case/test_homelink.py  --alluredir=Report/temp
    # allure generate Report/temp -o Report/allure_report --clean
